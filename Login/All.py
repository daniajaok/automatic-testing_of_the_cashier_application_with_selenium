import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# https://www.geeksforgeeks.org/writing-tests-using-selenium-python/?ref=lbp
class LoginTestCase(unittest.TestCase):
    # initialization of webdriver
    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.maximize_window()

    def test_verify_login_successfully(self):
        self.browser.get("https://kasirdemo.belajarqa.com/")
        WebDriverWait(self.browser, 10).until(EC.visibility_of_element_located((By.ID, "email")))

        self.browser.find_element(By.ID, "email").send_keys("MuhammadRamdhani@gmail.com")
        self.browser.find_element(By.ID, "password").send_keys("MuhammadRamdhani@gmail.com")
        self.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div[2]/div/button').click()
        WebDriverWait(self.browser, 10).until(EC.text_to_be_present_in_element((By.CLASS_NAME, 'chakra-stat__label'), "es teruss"))

    def test_verify_login_failed_account(self):
        self.browser.get("https://kasirdemo.belajarqa.com/")
        WebDriverWait(self.browser, 10).until(EC.visibility_of_element_located((By.ID, "email")))

        self.browser.find_element(By.ID, "email").send_keys("ayam@gmail.com")
        self.browser.find_element(By.ID, "password").send_keys("ayam@gmail.com")
        self.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div[2]/div/button').click()
        WebDriverWait(self.browser, 10).until(EC.text_to_be_present_in_element((By.XPATH, '//*[@role="alert"]'), "Kredensial yang Anda berikan salah"))

    def test_verify_login_failed_blank_field(self):
        self.browser.get("https://kasirdemo.belajarqa.com/")
        WebDriverWait(self.browser, 10).until(EC.visibility_of_element_located((By.ID, "email")))

        self.browser.find_element(By.ID, "email").send_keys("")
        self.browser.find_element(By.ID, "password").send_keys("")
        self.browser.find_element(By.XPATH, '//*[@id="root"]/div/div/div/div[2]/div/button').click()
        WebDriverWait(self.browser, 10).until(EC.text_to_be_present_in_element((By.XPATH, '//*[@role="alert"]'), '"email" is not allowed to be empty'))
    # cleanup method called after every test performed
    def tearDown(self):
        self.browser.quit()
if __name__ == '__main__':
    unittest.main()