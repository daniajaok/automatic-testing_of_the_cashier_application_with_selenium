# Aplikasi test aplikasi https://kasirdemo.belajarqa.com/ menggunakan selenium 


## Aplikasi kasirAja
Aplikasi kasirAja adalah sebuah perangkat lunak yang dirancang untuk membantu bisnis dalam melakukan transaksi penjualan dengan lebih efisien. Dengan menggunakan aplikasi kasirAja, pengguna dapat mencatat dan mengelola pembelian, penjualan, serta inventaris barang secara teratur dan terorganisir. Aplikasi ini biasanya dilengkapi dengan fitur-fitur seperti  pencatatan pembayaran, manajemen stok barang, dan pelaporan keuangan. Tujuan utama dari aplikasi kasir adalah untuk membantu pemilik bisnis dalam mengoptimalkan proses transaksi, meningkatkan akurasi pencatatan, dan memudahkan analisis performa bisnis secara keseluruhan.

## Keterangan
Dalam studi kasus ini penulis menguji Aplikasi kasirAja menggunakan alat pengujian selenium yaitu sumber terbuka dan otomatis. penulis akan pengujian meliputi fitur-fitur seagai berikut :
    
    1.Login 
    2.Daftar account
    3.dashboard
    4.penjualan
    5.pembelian
    6.etc
pada test ini akan diupdate secara berkala setiap 1 minggu sekali, bagi ingin melihat update terbaru silahka tekan commit terbaru 🙏

## cara setup & menjalankan
1. pip install -r requirements.txt
2. masuk folder yang di inginkan  
3. menjalankan dengan menuliskan py nama_file.py